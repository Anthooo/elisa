<?php

namespace ElisaBundle\Entity;

/**
 * Modele
 */
class Modele
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \ElisaBundle\Entity\Categorie
     */
    private $categorie;

    /**
     * @var \ElisaBundle\Entity\Image
     */
    private $image;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Modele
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set categorie
     *
     * @param \ElisaBundle\Entity\Categorie $categorie
     *
     * @return Modele
     */
    public function setCategorie(\ElisaBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \ElisaBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set image
     *
     * @param \ElisaBundle\Entity\Image $image
     *
     * @return Modele
     */
    public function setImage(\ElisaBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \ElisaBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }
}
