<?php

namespace ElisaBundle\Controller;

use ElisaBundle\Entity\Modele;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Modele controller.
 *
 */
class ModeleController extends Controller
{
    /**
     * Lists all modele entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $modeles = $em->getRepository('ElisaBundle:Modele')->findAll();

        return $this->render('@Elisa/Admin/modele/index.html.twig', array(
            'modeles' => $modeles,
        ));
    }

    /**
     * Creates a new modele entity.
     *
     */
    public function newAction(Request $request)
    {
        $modele = new Modele();
        $form = $this->createForm('ElisaBundle\Form\ModeleType', $modele);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $modele->getImage()->upload($modele->getImage()->files);

            $em->persist($modele);
            $em->flush();

            return $this->redirectToRoute('modele_index', array('id' => $modele->getId()));
        }

        return $this->render('@Elisa/Admin/modele/new.html.twig', array(
            'modele' => $modele,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing modele entity.
     *
     */
    public function editAction(Request $request, Modele $modele)
    {
        $editForm = $this->createForm('ElisaBundle\Form\ModeleType', $modele);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $modele->getImage()->upload($modele->getImage()->files);

            $em->persist($modele);
            $em->flush();

            return $this->redirectToRoute('modele_edit', array('id' => $modele->getId()));
        }

        return $this->render('@Elisa/Admin/modele/edit.html.twig', array(
            'modele' => $modele,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a cour entity.
     *
     */
    public function deleteAction($id)
    {
        if ($id) {
            $em = $this->getDoctrine()->getManager();
            $modele = $em->getRepository('ElisaBundle:Modele')->findOneBy(array('id' => $id));
            $image = $em->getRepository('ElisaBundle:Image')->findOneBy(array('id' => $modele->getImage()->getId()));
            $em->remove($modele);
            $modele->getImage()->removeUpload($image->getUrls());
            $em->flush();

            return $this->redirectToRoute('modele_index');
        } else
            return $this->redirectToRoute('modele_index');

    }

    /**
     * Deletes a image entity.
     *
     */
    public function deleteImgAction($id, $name)
    {
        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('ElisaBundle:Image')->findOneBy(array('id' => $id));

        $image->removeUpload($name);

        $em->flush();
        return $this->redirectToRoute('modele_index');
    }

}
