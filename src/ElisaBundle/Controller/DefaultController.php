<?php

namespace ElisaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ElisaBundle:Default:index.html.twig');
    }

    public function AdminAction ()
    {
        return $this->render('@Elisa/Admin/index.html.twig');
    }

    public function GalerieAction()
    {
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository("ElisaBundle:Modele")->findAll();
        $abouts = $em->getRepository('ElisaBundle:About')->findAll();
        return $this->render('@Elisa/Default/galerie.html.twig', array(
            'modeles'=>$modeles,
            'abouts'=>$abouts
        ));
    }

    public function contactAction()
    {
        $em = $this->getDoctrine()->getManager();
        $abouts = $em->getRepository('ElisaBundle:About')->findAll();

        return $this->render('@Elisa/Default/contact.html.twig', array(
            'abouts'=>$abouts
        ));
    }

    public function AboutAction()
    {
        $em = $this->getDoctrine()->getManager();
        $abouts = $em->getRepository("ElisaBundle:About")->findBy(array(), array('date'=>'desc'));;
        return $this->render('@Elisa/Default/about.html.twig', array(
            'abouts'=>$abouts
        ));
    }

    public function mailAction()
    {
        $em = $this->getDoctrine()->getManager();
        $abouts = $em->getRepository("ElisaBundle:About")->findAll();
        return $this->render('@Elisa/Default/confirm.html.twig', array(
            'abouts'=>$abouts
        ));
    }

    public function sendAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $abouts = $em->getRepository("ElisaBundle:About")->findAll();
        $from = $this->getParameter('mailer_user');
        $name = $request->request->get('nom');
        $mail = $request->request->get('email');
        $sujet = $request->request->get('Sujet');
        $message = $request->request->get('message');

        $message = \Swift_Message::newInstance()
            ->setSubject('Contact Site')
            ->setFrom(array($from => 'Elisabeth Creusot'))
            ->setTo($from)
            ->setBody(
                $this->renderView(
                    '@Elisa/Default/mail.html.twig',
                    array(
                        'nom' => $name,
                        'email' => $mail,
                        'sujet' => $sujet,
                        'message' => $message
                    )
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);
        return $this->render('@Elisa/Default/confirm.html.twig', array(
            'abouts'=>$abouts
        ));

    }

}
