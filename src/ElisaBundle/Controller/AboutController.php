<?php

namespace ElisaBundle\Controller;

use ElisaBundle\Entity\About;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * About controller.
 *
 */
class AboutController extends Controller
{
    /**
     * Lists all about entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $abouts = $em->getRepository('ElisaBundle:About')->findAll();

        return $this->render('@Elisa/Admin/about/index.html.twig', array(
            'abouts' => $abouts,
        ));
    }

    /**
     * Creates a new about entity.
     *
     */
    public function newAction(Request $request)
    {
        $about = new About();
        $form = $this->createForm('ElisaBundle\Form\AboutType', $about);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($about);
            $em->flush();

            return $this->redirectToRoute('about_index', array('id' => $about->getId()));
        }

        return $this->render('@Elisa/Admin/about/new.html.twig', array(
            'about' => $about,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing about entity.
     *
     */
    public function editAction(Request $request, About $about)
    {
        $editForm = $this->createForm('ElisaBundle\Form\AboutType', $about);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('about_index', array('id' => $about->getId()));
        }

        return $this->render('@Elisa/Admin/about/edit.html.twig', array(
            'about' => $about,
            'edit_form' => $editForm->createView(),
        ));
    }

}
