<?php

namespace ElisaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ElisaBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
